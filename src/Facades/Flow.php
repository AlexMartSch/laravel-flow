<?php

namespace AlexMart\LaravelFlow\Facades;

use Illuminate\Support\Facades\Facade;

class Flow extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \LaravelFlow\Flow::class;
    }
}
